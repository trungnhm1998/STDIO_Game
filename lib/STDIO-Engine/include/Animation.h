#pragma once
#include"Sprite.h"

class Animation : public Sprite
{
public:
	Animation();
	~Animation();

	void loadAnimation(const char* textureSourcePath, const char* configSourcePath, int fps, bool loop, int numberofFrames=0);
	void update(Uint32 dt);
	void playAnimation();
	void stopAnimation();
	virtual void draw();

private:
	bool _isPlay = false;
	SDL_Rect* _animationFrames;
	int _fps;// FPS for switch frame
	int _frameIndex;
	int _maxFrameIndex;
	bool _loop;
	Uint32 _frameTime;
};

