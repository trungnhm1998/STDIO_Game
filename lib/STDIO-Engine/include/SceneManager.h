#pragma once
#include"Scene.h"

class SceneManager
{
public:
	SceneManager();
	~SceneManager();
	static SceneManager* getInstance();
	Scene* getCurrentScene();
	void setStartScene(Scene* Scene);
	void updateScene(Uint32 dt);
	void switchScene(Scene* NextScene);

private:
	static SceneManager* _instance;
	Scene* _currentScene;
	Scene* _nextScene;

};

