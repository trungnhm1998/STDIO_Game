#pragma once
#include<stdio.h>
#include<SDL.h>
#include<SDL_image.h>
#include<SDL_mixer.h>
#include<SDL_ttf.h>

#include"SceneManager.h"

enum class ApplicationState
{
	Run,
	Exit
};

class Application
{
public:
	Application();
	~Application();
	void run(int fps);
	bool init(const char* windowTitle, int windowWidth, int windowHeight);
	static ApplicationState _applicationState;

	static int getScreenWidth();
	static int getScreenHeight();

private:
	//function
	SDL_Window* _window;
	void loop(Uint32 dt);
	void draw();
	void eventHandler();
	static int _screenWidth;
	static int _screenHeight;
	//variables
	int _fps;
};

