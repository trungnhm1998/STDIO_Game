#pragma once
#include"Text.h"
#include"InputManager.h"
#include<iostream>
#include<functional>

class Button
{
public:
	Button(const char * sourcePathNormalTexture, const char * sourcePathClickTexture, char* labelText, int labelSize);
	Button(const char * sourcePathNormalTexture, char* labelText, int labelSize);//init with no x y but have W and H Default size have to manually set pos. default pos is 0:0
	~Button();
	void update(float dt);
	void draw();
	void setButtonPosition(int x, int y);
	int getButtonHeight() const { return _buttonRect.h; }
	int getButtonWidth() const { return _buttonRect.w; }
	void setHoverTexture(const char* sourcePath);
	void setClickedTexture(const char* sourcePath);
	
	void setOnClickListener(std::function<void()> func);

	void setHoverSFX(const char* sourcePath);
	void setClickSFX(const char* sourcePath);

	SDL_Point getButtonPosition();

	//variables
	
	std::function<void()> _onClick;

private:
	enum State 
	{
		Click,
		Hover,
		Normal
	};

	State _state;
	bool _hoverSFXPlayed = false;
	bool _clickSFXPlayed = false;
	Texture* _normaltexture;
	Texture* _hoverTexture;
	Texture* _clickTexture;
	SDL_Rect _buttonRect;
	Text* _label;
};

