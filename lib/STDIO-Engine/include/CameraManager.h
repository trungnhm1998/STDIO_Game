#pragma once
#include<SDL.h>
class CameraManager
{
public:
	CameraManager();
	~CameraManager();
	static CameraManager* getInstance();

	SDL_Point getCameraPosition();
	void setCameraPosition(int x, int y);

private:
	int XOffSet = 0;
	int YOffSet = 0;
	static CameraManager* _instance;

};

