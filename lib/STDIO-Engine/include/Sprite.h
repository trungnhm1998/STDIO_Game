#pragma once
#define _CRT_SECURE_NO_WARNINGS
#include<SDL.h>
#include<GraphicManager.h>
#include<stdio.h>
#include <document.h>

class Sprite
{
public:
	Sprite();
	~Sprite();
	void loadTexture(const char* sourcePath);
	void loadTexture(const char* sourcePathTexture,const char* sourcePathConfig, const char* spriteName);

	void flipHorizontal();
	void flipVertical();

	void rotate(double angle);
	void setPosition(int x, int y);
	void setScale(float scale);

	void draw();

	void resetSprite();//reset flip and rotate angle

	SDL_Point getPosition();
	double getRotateAngle();
	int getTextureWidth();
	int getTextureHeight();

protected:
	double _rotateAngle;//hold rotate angle
	SDL_RendererFlip  _flipOption;// hold flip option
	Texture* _texture;//texture for sprite
	SDL_Rect _boundingBox;//Rectangle hold position and size of the texture AKA destinationRect
	SDL_Rect _sourceRect;
	int _scale;
	char* _configFileBuffer;

	
private:


};

