#pragma once
#include<SDL.h>
#include<SDL_image.h>
#include<stdio.h>

class Texture
{
public:
	Texture();
	~Texture();
	bool loadTextureFromPath(const char* sourcePath);
	SDL_Texture* getTexture();
	int getTextureWidth();
	int getTextureHeight();
	SDL_Point getCenterPoint();

	void setTexture(SDL_Texture* texture);

private:
	SDL_Point _centerPoint;
	SDL_Texture* _texture;
	SDL_Rect _size;
};

