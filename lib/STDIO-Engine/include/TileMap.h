#pragma once
#define _CRT_SECURE_NO_WARNINGS
#include <SDL.h>
#include <document.h>
#include "GraphicManager.h"
#include "Texture.h"



class TileMap
{
public:
	TileMap();
	~TileMap();
	void loadMap(const char* tileMapSourcepath);
	void drawLayer(const char* layerName);

private:
	//data structure for Layer and Tile Set
	struct TileLayer
	{
	public:
		char _name[100];
		int _width;
		int _height;
		int _x;
		int _y;
		float _opacity;
		bool _visible;
		int _offSetX;
		int _offSetY;
		int* _data;
		Texture _texture;
	};
	struct TileSets
	{
		char* _tileSetsTexPath;
		char* _tileSetsName;

		int _columns;
		int _firstGID;
		int _tileSetsTexWidth;
		int _tileSetsTexHeight;
		int _margin;
		int _tileCount;
		int _tileWidth;
		int _tileHeight;
	};

	//data structure for Map
	int _tileWidth;
	int _tileHeight;
	int _mapHeight;
	int _mapWidth;
	int _layerCount;
	TileLayer* _mapLayers;
	TileSets* _tileSets;
};

