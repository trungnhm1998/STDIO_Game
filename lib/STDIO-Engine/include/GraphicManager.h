#pragma once
#include"SDL.h"
#include"Texture.h"
#include "CameraManager.h"

class GraphicManager
{
public:
	GraphicManager();
	~GraphicManager();
	static GraphicManager* getInstance();

	bool init(SDL_Window* window);

	void setBackgroundColor(Uint8 r, Uint8 g, Uint8 b,Uint8 a);

	void setFillColor(Uint8 r, Uint8 g, Uint8 b, Uint8 a);

	void rendererClear();
	void rendererPresent();

	void drawRect(int x, int y, int width ,int height);
	void drawLine(SDL_Point vector1,SDL_Point vector2);
	void drawLines(SDL_Point* points, int numberOfPoints);

	void drawTexture(Texture* texture);//draw texture from existing Texture object
	void drawTexture(Texture* texture, SDL_Rect rect);
	void drawTexture(Texture* texture, SDL_RendererFlip flipOption,double rotateAngle, SDL_Rect rect);
	void drawTexture(Texture* texture,SDL_RendererFlip flipOption,double rotateAngle,SDL_Rect srcRect,SDL_Rect desRect);



	void close();

	SDL_Renderer* getRenderer();

private:

	//variables

	static GraphicManager* _instance;

	SDL_Renderer* _renderer;

	SDL_Color _defaultRendererColor;//default color for background

	SDL_Color _fillColor;
};

