#pragma once
#include<SDL.h>
#include<InputDefinition.h>
class InputManager
{
public:
	InputManager();
	~InputManager();
	//function
	static InputManager* getInstance();

	void setMousePosition(SDL_Point mousePosition);
	SDL_Point getMousePosition();

	void setMouseKeyStateUp(Uint8 mouseCode);
	void setMouseKeyStateDown(Uint8 mouseCode);

	bool getMouseKeyState(MouseKey keyCode);
	bool getMouseKeyDown(MouseKey keyCode);
	bool getMouseKeyUp(MouseKey keyCode);

	void setKeyStateUp(SDL_Scancode keyCode);
	void setKeyStateDown(SDL_Scancode keyCode);

	bool getKeyState(KeyboardKey keyCode);
	bool getKeyDown(KeyboardKey keyCode);
	bool getKeyUp(KeyboardKey keyCode);


private:
	//variables
	static InputManager* _instance;
	SDL_Point _mousePosition;
	bool _keysState[KEYBOARD_SIZE] = { false };
	bool _mouseKeyState[MOUSE_SIZE] = { false };
};

