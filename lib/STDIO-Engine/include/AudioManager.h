#pragma once
#include<iostream>
#include<vector>
#include<SDL_mixer.h>
class AudioManager
{
public:
	AudioManager();
	~AudioManager();
	 static AudioManager* getInstance();
	 void playSoundEffect(const char* sourcePath,int repeateTime);
	 // playoption true if you want music to repeat forever 
	 //fasle for play only one time
	 void playMusic(const char* sourcePath,bool playOption);
	 void pauseMusic();
	 void resumeMusic();
	 void stopMusic();
	 
	 //max value is 128 min value is 0
	 void setMusicVolume(int value);
	 //increase and decrease value by 1 each time
	 void increaseMusicVolume();
	 void decreaseMusicVolume();

	 void close();

private:
	static AudioManager* _instance;
	Mix_Music* _music;
	Mix_Chunk* _sfx;

	std::vector<Mix_Chunk*> _sfxVector; 

	int _musicVolume;
};

