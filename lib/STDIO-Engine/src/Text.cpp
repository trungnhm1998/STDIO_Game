#include "Text.h"

Text::Text()
{
	_textTexture = new Texture;
}

Text::~Text()
{
	TTF_CloseFont(_textFont);
	delete _textTexture;
	_textTexture = NULL;
}
Text::Text(char * text, char* font,int textSize)
{
	_textTexture = new Texture;
	_textString = text;
	_textSize = textSize;
	//init TTF lib to use TTF font support
	if (TTF_Init() == -1)
		printf("Text TTF init Failed! Error: %s\n", TTF_GetError());
	else
	{
		//load font
		_textFont = TTF_OpenFont(font, _textSize);

		if (_textFont == NULL)
		{
			printf("Failed to load font %s! Error: %s\n", font,TTF_GetError());
		}
		else
		{
			_fontPath = font;
			SDL_Surface* textSurface = TTF_RenderText_Solid(_textFont, _textString, _textColor);
			SDL_Texture* TempTexture = SDL_CreateTextureFromSurface(GraphicManager::getInstance()->getRenderer(), textSurface);


			_textTexture->setTexture(TempTexture);

			//get Text Size and set to default size
			TTF_SizeText(_textFont, _textString, &_textRect.w, &_textRect.h);

			SDL_FreeSurface(textSurface);
		}
	}
}




void Text::draw()
{
	GraphicManager::getInstance()->drawTexture(_textTexture, _textRect);
}

void Text::setText(char * text)
{
	_textString = text;
	SDL_Surface* textSurface = TTF_RenderText_Solid(_textFont, text, _textColor);

	_textTexture->setTexture(SDL_CreateTextureFromSurface(GraphicManager::getInstance()->getRenderer(), textSurface));

	//get Text Size and set to default size
	TTF_SizeText(_textFont, text, &_textRect.w, &_textRect.h);

	SDL_FreeSurface(textSurface);
}

void Text::setTextColor(Uint8 r, Uint8 g, Uint8 b)
{
	_textColor = { r,g,b };

	SDL_Surface* textSurface = TTF_RenderText_Solid(_textFont, _textString, _textColor);

	_textTexture->setTexture(SDL_CreateTextureFromSurface(GraphicManager::getInstance()->getRenderer(), textSurface));
}

void Text::setTextOpacity(Uint8  opacity)
{
	_textColor.a = opacity;

	SDL_Surface* textSurface = TTF_RenderText_Solid(_textFont, _textString, _textColor);

	_textTexture->setTexture(SDL_CreateTextureFromSurface(GraphicManager::getInstance()->getRenderer(), textSurface));
}

void Text::setTextSize(int textSize)
{
	_textFont = TTF_OpenFont(_fontPath, textSize);

	if (_textFont == NULL)
	{
		printf("Failed to load font %s! Error: %s\n", _fontPath, TTF_GetError());
	}
	else
	{
		SDL_Surface* textSurface = TTF_RenderText_Solid(_textFont, _textString, _textColor);
		SDL_Texture* TempTexture = SDL_CreateTextureFromSurface(GraphicManager::getInstance()->getRenderer(), textSurface);


		_textTexture->setTexture(TempTexture);

		//get Text Size and set to default size
		TTF_SizeText(_textFont, _textString, &_textRect.w, &_textRect.h);

		SDL_FreeSurface(textSurface);
	}
}
