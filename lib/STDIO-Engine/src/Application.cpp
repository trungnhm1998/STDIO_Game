#include"Application.h"
int Application::_screenWidth;
int Application::_screenHeight;
ApplicationState Application::_applicationState;

Application::Application()
{
	_applicationState = ApplicationState::Run;
	_fps = 0;
	_window = nullptr;
}

Application::~Application()
{
	_window = nullptr;
	SDL_DestroyWindow(_window);
	delete SceneManager::getInstance();
	delete InputManager::getInstance();
	delete GraphicManager::getInstance();
	delete AudioManager::getInstance();
}

void Application::run(int fps)
{
	_fps = fps;
	//if the start scene is not null the user have set they own Start Scene
	if (SceneManager::getInstance()->getCurrentScene() != nullptr)
	{
		//then start update

		//first init start scene
		SceneManager::getInstance()->getCurrentScene()->init();

		//see if the user want to stop or not if not keep looping game loop
		while (_applicationState != ApplicationState::Exit)
		{
			//init start time of the Tick/Frame
			static Uint32 FrameTime;
			Uint32 FrameStartTime = SDL_GetTicks();
			//start update

			//update Scene
			loop(FrameTime);

			//drawing Scene
			//clear SceneFirst
			GraphicManager::getInstance()->rendererClear();
			draw();//actual draw
			GraphicManager::getInstance()->rendererPresent();//update draw

			//self explained
			eventHandler();

			//end update
			//caulate Tick/Frame time after all the draw update event
			FrameTime = SDL_GetTicks() - FrameStartTime;

			//if Tick/frame < than the delay time 
			if (FrameTime < (1000 / _fps))//1 second / FPS -> ms in 1 frame
			{
				//delay the time
				SDL_Delay((1000 / _fps) - FrameTime);
				FrameTime += ((1000 / _fps) - FrameTime);
			}
		}

		//free Engine componets
		AudioManager::getInstance()->close();
		GraphicManager::getInstance()->close();

		//free SDL components
		Mix_Quit();
		IMG_Quit();
		SDL_Quit();

	}
	else
	{

		printf("Start Scene is NULL!\n");
		SDL_Delay(5000);
		SDL_Quit();

	}
}

bool Application::init(const char* windowTitle, int windowWidth, int windowHeight)
{
	if (SDL_Init(SDL_INIT_EVERYTHING) < 0)
	{
		printf("SDL Could not init! Error: %s\n", SDL_GetError());
		return false;
	}
	else
	{
		printf("SDL Init Successful!\n");
		_screenWidth = windowWidth;
		_screenHeight = windowHeight;
		_window = SDL_CreateWindow(windowTitle, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, windowWidth, windowHeight, SDL_WINDOW_SHOWN);
		if (_window == nullptr)
		{
			printf("SDL Window could not init! Error: %s\n", SDL_GetError());
			return false;
		}
		else
		{
			printf("SDL Window init Successful!\n");
			
			//init Renderer for Graphic to draw from GPU
			if (!GraphicManager::getInstance()->init(_window))
			{
				printf("Graphic renderer could not init! Error: %s\n", SDL_GetError());
				return false;
			}
			else
			{
				printf("Graphic renderer init sucessful!\n");
				int IMG_Flag = IMG_INIT_PNG || IMG_INIT_JPG;
				if (!(IMG_Init(IMG_Flag) & IMG_Flag))
				{
					printf("SDL Image Extention Support Could not Init! Error: %s\n", IMG_GetError());
					return false;
				}
				else
					printf("SDL Image Extention Support init successful!\n");

				//init Mixer for audio support
				if (Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 2048) < 0)
				{
					printf("SDL_mixer could not initialize! SDL_mixer Error: %s\n", Mix_GetError());
					return false;
				}
				else
				{
					printf("SDL_mixer initialize successful!\n");
					if (Mix_Init(MIX_INIT_MP3) == 0)
					{
						printf("SDL_Mixer MP3 Format could not initialize! SDL_mixer Error: %s\n",Mix_GetError());
						return false;
					}
					else
						printf("SDL_mixer MP3 format initialize successful!\n");
				}

			}
		}
	}

	return true;
}

int Application::getScreenWidth()
{
	return _screenWidth;
}

int Application::getScreenHeight()
{
	return _screenHeight;
}

void Application::eventHandler()
{
	SDL_Event Event;

	while (SDL_PollEvent(&Event))
	{
		switch (Event.type)
		{
		case SDL_QUIT:
			_applicationState = ApplicationState::Exit;
			break;
		case SDL_MOUSEMOTION:
			InputManager::getInstance()->setMousePosition({ Event.motion.x,Event.motion.y });
			break;
		case SDL_KEYDOWN:
			InputManager::getInstance()->setKeyStateDown(Event.key.keysym.scancode);
			break;
		case SDL_KEYUP:
			InputManager::getInstance()->setKeyStateUp(Event.key.keysym.scancode);
			break;
		case SDL_MOUSEBUTTONDOWN:
			InputManager::getInstance()->setMouseKeyStateDown(Event.button.button);
			break;
		case SDL_MOUSEBUTTONUP:
			InputManager::getInstance()->setMouseKeyStateUp(Event.button.button);
			break;
		default:
			break;
		}
	}
}

void Application::loop(Uint32 frameTime)
{


	if (SceneManager::getInstance()->getCurrentScene() != nullptr)
		SceneManager::getInstance()->updateScene(frameTime);


}

void Application::draw()
{
	if (SceneManager::getInstance()->getCurrentScene() != nullptr)
		SceneManager::getInstance()->getCurrentScene()->draw();

}