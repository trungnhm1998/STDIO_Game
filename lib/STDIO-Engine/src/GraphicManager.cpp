#include"GraphicManager.h"
#include<stdio.h>
GraphicManager* GraphicManager::_instance;

GraphicManager::GraphicManager()
{
	_defaultRendererColor = { 255,255,255,255 };
	_fillColor = { 0,0,0,255 };
	_renderer = NULL;

}

GraphicManager::~GraphicManager()
{
	delete CameraManager::getInstance();
	_renderer = NULL;
}

GraphicManager * GraphicManager::getInstance()
{
	if (_instance == nullptr)
		_instance = new GraphicManager();
	return _instance;
}

bool GraphicManager::init(SDL_Window * window)
{
	_renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
	if (_renderer != nullptr)
	{
		SDL_SetRenderDrawColor(_renderer, _defaultRendererColor.r, _defaultRendererColor.g, _defaultRendererColor.b, _defaultRendererColor.a);
		return true;
	}

	return false;
}

void GraphicManager::setBackgroundColor(Uint8 r, Uint8 g, Uint8 b, Uint8 a)
{
	_defaultRendererColor = { r,g,b,a };
}

void GraphicManager::setFillColor(Uint8 r, Uint8 g, Uint8 b, Uint8 a)
{
	_fillColor = { r,g,b,a };
}

void GraphicManager::drawRect(int x, int y, int width, int height)
{
	SDL_Rect TempRect;
	TempRect.x = x -CameraManager::getInstance()->getCameraPosition().x;
	TempRect.y = y + CameraManager::getInstance()->getCameraPosition().y;
	TempRect.w = width;
	TempRect.h = height;

	//draw Rect
	SDL_RenderDrawRect(_renderer, &TempRect);
	SDL_SetRenderDrawColor(_renderer, _fillColor.r, _fillColor.g, _fillColor.b, _fillColor.a);
	SDL_RenderFillRect(_renderer, &TempRect);


}

void GraphicManager::drawLine(SDL_Point vector1, SDL_Point vector2)
{
	SDL_Point Tvec1 = {vector1.x - CameraManager::getInstance()->getCameraPosition().x ,vector1.y + CameraManager::getInstance()->getCameraPosition().y};
	SDL_Point Tvec2 = { vector2.x - CameraManager::getInstance()->getCameraPosition().x ,vector2.y + CameraManager::getInstance()->getCameraPosition().y };

	SDL_SetRenderDrawColor(_renderer, _fillColor.r, _fillColor.g, _fillColor.b, _fillColor.a);
	SDL_RenderDrawLine(_renderer, Tvec1.x, Tvec1.y, Tvec2.x, Tvec2.y);
}

void GraphicManager::drawLines(SDL_Point* points, int numberOfPoints)
{
	SDL_Point* Temp = new SDL_Point[numberOfPoints];
	for (int i = 0; i < numberOfPoints; i++)
	{
		Temp[i].x = points[i].x - CameraManager::getInstance()->getCameraPosition().x;
		Temp[i].y = points[i].y + CameraManager::getInstance()->getCameraPosition().y;
	}
	SDL_SetRenderDrawColor(_renderer, _fillColor.r, _fillColor.g, _fillColor.b, _fillColor.a);
	SDL_RenderDrawLines(_renderer, Temp, numberOfPoints);
	delete[] Temp;
}


void GraphicManager::drawTexture(Texture * texture)
{
	
	if (texture != nullptr)
	{
		
		SDL_Rect TempRect = {0,0,texture->getTextureWidth() ,texture->getTextureHeight()};
		TempRect.x =  CameraManager::getInstance()->getCameraPosition().x;
		TempRect.y = CameraManager::getInstance()->getCameraPosition().y;

		SDL_RenderCopy(_renderer, texture->getTexture(), NULL, &TempRect);
	}
	else
		printf("Texture is NULL failed!\n");
}

void GraphicManager::drawTexture(Texture * texture, SDL_Rect rect)
{

	if (texture != nullptr)
	{
		SDL_Rect TempRect = rect;
		TempRect.x = rect.x - CameraManager::getInstance()->getCameraPosition().x;
		TempRect.y = rect.y - CameraManager::getInstance()->getCameraPosition().y;

		SDL_RenderCopy(_renderer, texture->getTexture(), NULL, &TempRect);
	}
	else
		printf("Texture is NULL failed!\n");
}

void GraphicManager::drawTexture(Texture * texture, SDL_RendererFlip flipOption, double rotateAngle, SDL_Rect rect)
{
	SDL_RenderCopyEx(_renderer, texture->getTexture(), NULL, &rect, rotateAngle, NULL, flipOption);
}

void GraphicManager::drawTexture(Texture * texture, SDL_RendererFlip flipOption, double rotateAngle, SDL_Rect srcRect, SDL_Rect desRect)
{
	SDL_Rect TempRect = desRect;
	TempRect.x = desRect.x - CameraManager::getInstance()->getCameraPosition().x;
	TempRect.y = desRect.y - CameraManager::getInstance()->getCameraPosition().y;

	SDL_RenderCopyEx(_renderer, texture->getTexture(),&srcRect, &TempRect, rotateAngle, NULL, flipOption);
}

void GraphicManager::close()
{
	SDL_DestroyRenderer(_renderer);
}



SDL_Renderer * GraphicManager::getRenderer()
{
	return _renderer;
}

void GraphicManager::rendererClear()
{
	SDL_SetRenderDrawColor(_renderer, _defaultRendererColor.r, _defaultRendererColor.g, _defaultRendererColor.b, _defaultRendererColor.a);
	SDL_RenderClear(_renderer);
}

void GraphicManager::rendererPresent()
{
	SDL_RenderPresent(_renderer);
}
