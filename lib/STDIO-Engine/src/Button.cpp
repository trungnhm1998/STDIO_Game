#include "Button.h"

Button::Button(const char * sourcePathNormalTexture, const char * sourcePathClickTexture, char* labelText, int labelSize)
{
	_hoverTexture = NULL;
	_clickTexture = NULL;
	_normaltexture = NULL;
	_state = State::Normal;

	_label = new Text(labelText, "arial.ttf", labelSize);
	_normaltexture = new Texture;
	_normaltexture->loadTextureFromPath(sourcePathNormalTexture);

	_buttonRect.w = _normaltexture->getTextureWidth();
	_buttonRect.h = _normaltexture->getTextureHeight();

	_clickTexture = new Texture;
	_clickTexture->loadTextureFromPath(sourcePathClickTexture);

	_label->setTextPosition(_buttonRect.w / 2 - _label->getTextRect().w / 2, _buttonRect.h / 2 - _label->getTextRect().h / 2);
}
Button::Button(const char * sourcePathNormalTexture, char* labelText, int labelSize)
{
	_hoverTexture = NULL;
	_clickTexture = NULL;
	_normaltexture = NULL;
	_state = State::Normal;

	_label = new Text(labelText, "arial.ttf", labelSize);
	_normaltexture = new Texture;
	_normaltexture->loadTextureFromPath(sourcePathNormalTexture);

	_buttonRect.w = _normaltexture->getTextureWidth();
	_buttonRect.h = _normaltexture->getTextureHeight();


}


Button::~Button()
{
	_hoverTexture = NULL;
	_clickTexture = NULL;
	_normaltexture = NULL;

	delete _label;
	delete _clickTexture;
	delete _hoverTexture;
	delete _normaltexture;
}

void Button::update(float dt)
{
	switch (_state)
	{
	case Button::Click:
		if (_onClick != nullptr)
		{
			_onClick();
			_state = State::Hover;
		}
		break;
	case Button::Hover:
		break;
	case Button::Normal:
		break;
	default:
		break;
	}

	if (InputManager::getInstance()->getMousePosition().x >= _buttonRect.x &&
		InputManager::getInstance()->getMousePosition().x <= _buttonRect.x + _buttonRect.w &&
		InputManager::getInstance()->getMousePosition().y >= _buttonRect.y &&
		InputManager::getInstance()->getMousePosition().y <= _buttonRect.y + _buttonRect.h)
	{
		_state = State::Hover;
		if (InputManager::getInstance()->getMouseKeyState(MouseKey::KEY_MOUSE_LEFT))
		{
			_state = State::Click;
		}
	}
	else
		_state = State::Normal;


}

void Button::draw()
{
	switch (_state)
	{
	case Button::Click:
		if (_clickTexture != NULL)
		{
			GraphicManager::getInstance()->drawTexture(_clickTexture, _buttonRect);
		}
		else
			GraphicManager::getInstance()->drawTexture(_normaltexture, _buttonRect);
		break;
	case Button::Hover:
		if (_hoverTexture != NULL)
		{
			GraphicManager::getInstance()->drawTexture(_hoverTexture, _buttonRect);
		}
		else
			GraphicManager::getInstance()->drawTexture(_normaltexture, _buttonRect);
		break;
	case Button::Normal:
		GraphicManager::getInstance()->drawTexture(_normaltexture, _buttonRect);
		break;
	default:
		break;
	}
	_label->draw();
}

void Button::setButtonPosition(int x, int y)
{
	_buttonRect.x = x;
	_buttonRect.y = y;
	_label->setTextPosition(_buttonRect.x + (_buttonRect.w / 2 - _label->getTextRect().w / 2), _buttonRect.y + (_buttonRect.h / 2 - _label->getTextRect().h / 2));
}

void Button::setHoverTexture(const char * sourcePath)
{

}

void Button::setClickedTexture(const char * sourcePath)
{
}

void Button::setOnClickListener(std::function<void()> func)
{
	_onClick = func;
}


void Button::setHoverSFX(const char * sourcePath)
{

}

void Button::setClickSFX(const char * sourcePath)
{

}

SDL_Point Button::getButtonPosition()
{
	return{_buttonRect.x,_buttonRect.y};
}
