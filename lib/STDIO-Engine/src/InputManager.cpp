#include "InputManager.h"
#include<stdio.h>
InputManager* InputManager::_instance = nullptr;
InputManager::InputManager()
{
	for (int i = 0; i < KEYBOARD_SIZE; i++)
	{
		_keysState[i] = NULL;
	}

}

InputManager::~InputManager()
{
}

InputManager * InputManager::getInstance()
{
	if (_instance == nullptr)
		_instance = new InputManager();
	

	return _instance;
}

void InputManager::setMousePosition(SDL_Point mousePosition)
{
	_mousePosition = mousePosition;
}

SDL_Point InputManager::getMousePosition()
{
	return _mousePosition;
}

void InputManager::setMouseKeyStateUp(Uint8 mouseCode)
{
	_mouseKeyState[mouseCode] = false;
}

void InputManager::setMouseKeyStateDown(Uint8 mouseCode)
{
	_mouseKeyState[mouseCode] = true;
}

bool InputManager::getMouseKeyState(MouseKey keyCode)
{
	if (_mouseKeyState[keyCode])
	{
		_mouseKeyState[keyCode] = false;
		return true;
	}
	return false;
}

bool InputManager::getMouseKeyDown(MouseKey keyCode)
{
	return _mouseKeyState[keyCode];
}

bool InputManager::getMouseKeyUp(MouseKey keyCode)
{
	return !_mouseKeyState[keyCode];
}

void InputManager::setKeyStateUp(SDL_Scancode keyCode)
{
	_keysState[keyCode] = false;
}

void InputManager::setKeyStateDown(SDL_Scancode keyCode)
{
	_keysState[keyCode] = true;
}

bool InputManager::getKeyState(KeyboardKey keyCode)
{
	
	if (_keysState[keyCode])
	{
		_keysState[keyCode] = false;
		return true;
	}
	return false;
}

bool InputManager::getKeyDown(KeyboardKey keyCode)
{
	return _keysState[keyCode];
}

bool InputManager::getKeyUp(KeyboardKey keyCode)
{
	return !_keysState[keyCode];
}



