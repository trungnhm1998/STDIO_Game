#include"Sprite.h"


Sprite::Sprite()
{
	_configFileBuffer = nullptr;
	_scale = 1;
	_rotateAngle = 0;
	_flipOption = SDL_FLIP_NONE;
	_boundingBox = { 0 };
	_texture = new Texture();
}

Sprite::~Sprite()
{
	SDL_DestroyTexture(_texture->getTexture());
	delete[] _configFileBuffer;
	delete _texture;

	_configFileBuffer = nullptr;
	_texture = nullptr;
}

void Sprite::loadTexture(const char * sourcePath)
{
	if (!_texture->loadTextureFromPath(sourcePath))
		printf("Cannot load texture!\n");
	else
	{
		_boundingBox.x = 0;
		_boundingBox.y = 0;
		_boundingBox.w = _texture->getTextureWidth();
		_boundingBox.h = _texture->getTextureHeight();
		_sourceRect = _boundingBox;
	}
}

//load texture to _texture (Texture class) and source Rect
void Sprite::loadTexture(const char * sourcePath, const char* sourcePathConfig, const char* spriteName)
{
	if (!_texture->loadTextureFromPath(sourcePath))
		printf("Cannot load texture file!\n");
	else
	{
		long fileSize;
		rapidjson::Document JsonDocument;

		if (_configFileBuffer == nullptr)
		{

			FILE* file = fopen(sourcePathConfig, "rb");

			if (file == NULL)
			{
				printf("Cannot open config file!\n");
			}

			fseek(file, 0, SEEK_END);
			fileSize = ftell(file) + 1;
			fseek(file, 0, SEEK_SET);
			//create char array have size = file size
			_configFileBuffer = new char[fileSize];

			fread(_configFileBuffer, 1, fileSize, file);//copy file char* -> char* arry

			_configFileBuffer[fileSize - 1] = 0;
			fclose(file);
		}


		if (JsonDocument.Parse(_configFileBuffer).HasParseError())
		{
			SDL_Log("can't parse file! error: %d\n", JsonDocument.Parse(_configFileBuffer).GetParseError());
		}
		else
		{
			//printf("%s\n", buffer);
			assert(JsonDocument.IsObject());
			assert(JsonDocument.HasMember("frames"));
			if (JsonDocument["frames"].HasMember(spriteName))
			{
				_sourceRect = { JsonDocument["frames"][spriteName]["x"].GetInt(),
					JsonDocument["frames"][spriteName]["y"].GetInt(),
					JsonDocument["frames"][spriteName]["w"].GetInt(),
					JsonDocument["frames"][spriteName]["h"].GetInt() };
				_boundingBox = { _boundingBox.x,_boundingBox.y ,JsonDocument["frames"][spriteName]["sw"].GetInt(),JsonDocument["frames"][spriteName]["sh"].GetInt() };
			}
			else
			{
				SDL_Log("Cannot load sprite from file name: %s! jsonParse cannot find id\n", spriteName);
			}

			
			//printf("%s", buffer);

		}
	}
}

void Sprite::flipHorizontal()
{
	if (_flipOption == SDL_FLIP_NONE)
		_flipOption = SDL_FLIP_HORIZONTAL;
	else if (_flipOption == SDL_FLIP_HORIZONTAL)
		_flipOption = SDL_FLIP_NONE;
	else if (_flipOption == SDL_FLIP_VERTICAL)
		_flipOption = (SDL_RendererFlip)(SDL_FLIP_VERTICAL | SDL_FLIP_HORIZONTAL);
	else if (_flipOption == (SDL_RendererFlip)(SDL_FLIP_VERTICAL | SDL_FLIP_HORIZONTAL))
		_flipOption = SDL_FLIP_VERTICAL;
}

void Sprite::flipVertical()
{
	if (_flipOption == SDL_FLIP_NONE)
		_flipOption = SDL_FLIP_VERTICAL;
	else if (_flipOption == SDL_FLIP_VERTICAL)
		_flipOption = SDL_FLIP_NONE;
	else if (_flipOption == SDL_FLIP_HORIZONTAL)
		_flipOption = (SDL_RendererFlip)(SDL_FLIP_VERTICAL | SDL_FLIP_HORIZONTAL);
	else if (_flipOption == (SDL_RendererFlip)(SDL_FLIP_VERTICAL | SDL_FLIP_HORIZONTAL))
		_flipOption = SDL_FLIP_HORIZONTAL;
}

void Sprite::rotate(double angle)
{
	_rotateAngle = angle;
}

void Sprite::setPosition(int x, int y)
{
	_boundingBox.x = x;
	_boundingBox.y = y;
}

void Sprite::setScale(float scale)
{
	_scale = scale;
}

void Sprite::draw()
{
	GraphicManager::getInstance()->drawTexture(_texture, _flipOption, _rotateAngle, _sourceRect, { _boundingBox.x, _boundingBox.y ,_boundingBox.w * _scale,_boundingBox.h * _scale });
}


void Sprite::resetSprite()
{
	_flipOption = SDL_FLIP_NONE;
	_rotateAngle = 0;
}

SDL_Point Sprite::getPosition()
{
	return{ _boundingBox.x,_boundingBox.y };
}

double Sprite::getRotateAngle()
{
	return _rotateAngle;
}

int Sprite::getTextureWidth()
{
	return _texture->getTextureWidth();
}

int Sprite::getTextureHeight()
{
	return _texture->getTextureHeight();
}
