#include "Animation.h"

Animation::Animation()
{
	_scale = 1;
	_rotateAngle = 0;
	_flipOption = SDL_FLIP_NONE;
	_boundingBox = { 0 };
	_frameIndex = 0;
	_texture = new Texture();
	_loop = false;
}

Animation::~Animation()
{
	SDL_DestroyTexture(_texture->getTexture());
	delete[] _configFileBuffer;
	delete _texture;
	delete _animationFrames;

	_configFileBuffer = nullptr;

}

void Animation::loadAnimation(const char * textureSourcePath, const char * configSourcePath, int fps, bool loop, int numberofFrames)
{
	if (!_texture->loadTextureFromPath(textureSourcePath))
		printf("Cannot load texture file!\n");
	else
	{
		_fps = fps;
		_frameTime = 1000 / _fps;
		_loop = loop;
		rapidjson::Document _animationDocument;

		if (_configFileBuffer == nullptr)
		{
			long fileSize;

			FILE* file = fopen(configSourcePath, "rb");

			if (file == NULL)
			{
				printf("Cannot open FIle\n");
			}

			fseek(file, 0, SEEK_END);
			fileSize = ftell(file) + 1;
			fseek(file, 0, SEEK_SET);
			//create char array have size = file size
			_configFileBuffer = new char[fileSize];

			fread(_configFileBuffer, 1, fileSize, file);//copy file char* -> char* arry

			_configFileBuffer[fileSize - 1] = 0;
			fclose(file);
		}


		if (_animationDocument.Parse(_configFileBuffer).HasParseError())
		{
			printf("can't parse file! error: %d\n", _animationDocument.Parse(_configFileBuffer).GetParseError());
		}
		else
		{

			assert(_animationDocument.IsObject());
			assert(_animationDocument.HasMember("frames"));
			if ((!_animationDocument["frames"].ObjectEmpty()) && (numberofFrames == 0))// if there is any data
			{
				rapidjson::Value::ConstMemberIterator FirstMember = _animationDocument["frames"].MemberBegin();

				_sourceRect = { FirstMember->value["x"].GetInt(),
					FirstMember->value["y"].GetInt(),
					FirstMember->value["w"].GetInt(),
					FirstMember->value["h"].GetInt() };
				_boundingBox = { _boundingBox.x,_boundingBox.y
					,FirstMember->value["sw"].GetInt(),FirstMember->value["sh"].GetInt() };

				_animationFrames = new SDL_Rect[_animationDocument["frames"].MemberCount()];

				_maxFrameIndex = _animationDocument["frames"].MemberCount();
				int i = 0;
				for (rapidjson::Value::ConstMemberIterator itr = _animationDocument["frames"].MemberBegin(); itr != _animationDocument["frames"].MemberEnd(); itr++)
				{
					_animationFrames[i] = { itr->value["x"].GetInt() ,itr->value["y"].GetInt() ,itr->value["w"].GetInt() ,itr->value["h"].GetInt() };
					//SDL_Log("%d,%d,%d,%d", itr->value["x"].GetInt(), itr->value["y"].GetInt(), itr->value["w"].GetInt(), itr->value["h"].GetInt());
					i++;
				}

			}
			else if ((!_animationDocument["frames"].ObjectEmpty()) && (numberofFrames > 0))
			{
				rapidjson::Value::ConstMemberIterator FirstMember = _animationDocument["frames"].MemberBegin();

				_sourceRect = { FirstMember->value["x"].GetInt(),
					FirstMember->value["y"].GetInt(),
					FirstMember->value["w"].GetInt(),
					FirstMember->value["h"].GetInt() };
				_boundingBox = { _boundingBox.x,_boundingBox.y
					,FirstMember->value["sw"].GetInt(),FirstMember->value["sh"].GetInt() };

				_animationFrames = new SDL_Rect[_animationDocument["frames"].MemberCount()];

				_maxFrameIndex = numberofFrames;
				int i = 0;
				for (rapidjson::Value::ConstMemberIterator itr = _animationDocument["frames"].MemberBegin(); i < numberofFrames; itr++, i++)
				{
					_animationFrames[i] = { itr->value["x"].GetInt() ,itr->value["y"].GetInt() ,itr->value["w"].GetInt() ,itr->value["h"].GetInt() };
					//SDL_Log("%d,%d,%d,%d", itr->value["x"].GetInt(), itr->value["y"].GetInt(), itr->value["w"].GetInt(), itr->value["h"].GetInt());
				}
			}
			else
				SDL_Log("Cannot find Animation Name: %s\n");

			//printf("%s", buffer);

		}

	}


}


void Animation::update(Uint32 dt)
{
	if (_isPlay)
	{
		static Uint32 Time = 0;
		Time += dt;
		//SDL_Log("%d-%d", Time, _frameTime);
		if (Time >= _frameTime)
		{
			Time = 0;
			if (_frameIndex >= _maxFrameIndex && _loop)
				_frameIndex = 0;
			if (_frameIndex < _maxFrameIndex)
			{
				_sourceRect = _animationFrames[_frameIndex];
				_boundingBox.w = _animationFrames[_frameIndex].w;
				_boundingBox.h = _animationFrames[_frameIndex].h;
				_frameIndex++;
			}
		}
	}
}

void Animation::playAnimation()
{
	_isPlay = true;
}

void Animation::stopAnimation()
{
	_isPlay = false;
}

void Animation::draw()
{
	GraphicManager::getInstance()->drawTexture(_texture, _flipOption, _rotateAngle, _sourceRect, { _boundingBox.x, _boundingBox.y ,_boundingBox.w * _scale,_boundingBox.h * _scale });
}
