#include "CameraManager.h"

CameraManager* CameraManager::_instance;

CameraManager::CameraManager()
{
}

CameraManager::~CameraManager()
{
}

CameraManager * CameraManager::getInstance()
{
	if (_instance == nullptr)
		_instance = new CameraManager();
	return _instance;
}

SDL_Point CameraManager::getCameraPosition()
{
	return {XOffSet,YOffSet};
}

void CameraManager::setCameraPosition(int x, int y)
{
	XOffSet = x;
	YOffSet = y;
}
