#include"SceneManager.h"
SceneManager* SceneManager::_instance = nullptr;

SceneManager::SceneManager()
{
	_currentScene = NULL;
	_nextScene = NULL;
}

SceneManager::~SceneManager()
{
	if (_currentScene != nullptr)
		delete _currentScene;
}

SceneManager* SceneManager::getInstance()
{
	if (_instance == nullptr)
		_instance = new SceneManager();

	return _instance;
}

Scene * SceneManager::getCurrentScene()
{
	return _currentScene;
}

void SceneManager::setStartScene(Scene * Scene)
{
	_currentScene = Scene;
}

void SceneManager::updateScene(Uint32 dt)
{
	if (_currentScene == nullptr)
		return;
	if (_nextScene != nullptr)
	{
		delete _currentScene;

		_currentScene = _nextScene;
		_currentScene->init();
		_nextScene = nullptr;
	}

	_currentScene->update(dt);
}

void SceneManager::switchScene(Scene * NextScene)
{
	if (NextScene != nullptr)
		_nextScene = NextScene;
}
