#include "TileMap.h"
TileMap::TileMap()
{
}

TileMap::~TileMap()
{
	for (int i = 0; i < _layerCount; i++)
	{
		delete[] _mapLayers[i]._data;
	}
	delete[] _mapLayers;
	delete[] _tileSets;
}

void TileMap::loadMap(const char * tileMapSourcepath)
{
	rapidjson::Document TileMapDocument;
	char* Buffer;

	long fileSize;

	FILE* file = fopen(tileMapSourcepath, "rb");

	if (file == NULL)
	{
		printf("Cannot open FIle\n");
	}

	fseek(file, 0, SEEK_END);
	fileSize = ftell(file) + 1;
	fseek(file, 0, SEEK_SET);
	//create char array have size = file size
	Buffer = new char[fileSize];

	fread(Buffer, 1, fileSize, file);//copy file char* -> char* arry

	Buffer[fileSize - 1] = 0;
	fclose(file);

	if (TileMapDocument.Parse(Buffer).HasParseError())
	{
		SDL_Log("Can't read TileMap Json file!\n");
	}
	else
	{
		assert(TileMapDocument.IsObject());
		assert(TileMapDocument.HasMember("layers"));
		assert(TileMapDocument.HasMember("tilesets"));

		//init layers array to hold layer
		rapidjson::Value& Layers = TileMapDocument["layers"];
		_mapLayers = new TileLayer[TileMapDocument["layers"].Size()];
		_layerCount = TileMapDocument["layers"].Size();
		for (int i = 0; i < TileMapDocument["layers"].Size(); i++)
		{
			strcpy(_mapLayers[i]._name, Layers[i]["name"].GetString());//copy value name from json file to Layer
			_mapLayers[i]._width = Layers[i]["width"].GetInt();
			_mapLayers[i]._height = Layers[i]["height"].GetInt();
			_mapLayers[i]._x = Layers[i]["x"].GetInt();
			_mapLayers[i]._y = Layers[i]["y"].GetInt();
			if (Layers[i].HasMember("offsetx") && Layers[i].HasMember("offsety"))
			{
				_mapLayers[i]._offSetX = Layers[i]["offsetx"].GetInt();
				_mapLayers[i]._offSetY = Layers[i]["offsety"].GetInt();
			}
			_mapLayers[i]._opacity = Layers[i]["opacity"].GetFloat();
			_mapLayers[i]._visible = Layers[i]["visible"].GetBool();

			if (Layers[i].HasMember("image"))
			{
				_mapLayers[i]._texture.loadTextureFromPath(Layers[i]["image"].GetString());
			}
			if (Layers[i].HasMember("data"))
			{
				rapidjson::Value& Data = Layers[i]["data"];
				_mapLayers[i]._data = new int[Data.Size()];
				for (int j = 0; j < Data.Size(); j++)
				{
					_mapLayers[i]._data[j] = Data[j].GetInt();
				}
			}
			
		}


		//init tileset array to hold tile sets
		_tileSets = new TileSets[TileMapDocument["tilesets"].Size()];

	}
	delete[] Buffer;
}

void TileMap::drawLayer(const char * layerName)
{
	int i = 0;
	for ( ; i < _layerCount; i++)
	{
		if (strcmp(_mapLayers[i]._name, layerName) == 0)
		{
			GraphicManager::getInstance()->drawTexture(&(_mapLayers[i]._texture));
			break;
		}
	}
}
