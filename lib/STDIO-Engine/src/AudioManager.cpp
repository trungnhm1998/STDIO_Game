#include"AudioManager.h"
#include<stdio.h>

AudioManager* AudioManager::_instance = nullptr;

AudioManager::AudioManager()
{
	_musicVolume = 128;
	_music = NULL;
	_sfx = NULL;
}

AudioManager::~AudioManager()
{
	_sfx = NULL;
	_music = NULL;
}

AudioManager * AudioManager::getInstance()
{
	if (_instance == NULL)
		_instance = new AudioManager();
	return _instance;
}

void AudioManager::playSoundEffect(const char * sourcePath, int repeateTime)
{
	if (_sfx != NULL)
	{
		Mix_FreeChunk(_sfx);
		_sfx = Mix_LoadWAV(sourcePath);
	}
	else
		_sfx = Mix_LoadWAV(sourcePath);

	if (_sfx == NULL)
	{
		printf("Could not load WAV file path source! Erorr: %s", Mix_GetError());
	}
	else
		Mix_PlayChannel(-1, _sfx, repeateTime);
	

}

void AudioManager::playMusic(const char * sourcePath, bool playOption)
{
	//load Music from File
	if (Mix_PlayingMusic())
	{
		Mix_FreeMusic(_music);
	}
	_music = Mix_LoadMUS(sourcePath);

	if (_music == NULL)
	{
		printf("Could not load Music file path source! Error: %s", Mix_GetError());
	}
	else if (Mix_PlayingMusic() == 0)
	{
		Mix_VolumeMusic(_musicVolume);
		if (playOption)
			Mix_PlayMusic(_music, -1);
		else
			Mix_PlayMusic(_music, 0);
	}
}

void AudioManager::pauseMusic()
{
	if (Mix_PausedMusic() == 0 && Mix_PlayingMusic())
		Mix_PauseMusic();
}

void AudioManager::resumeMusic()
{
	if (Mix_PausedMusic() == 1 && Mix_PlayingMusic())
		Mix_ResumeMusic();
}

void AudioManager::stopMusic()
{
	if (Mix_PlayingMusic() != 0)
	{
		Mix_HaltMusic();
		Mix_FreeMusic(_music);
		_music = NULL;
	}
}

void AudioManager::setMusicVolume(int value)
{
	if (value > 128)
	{
		_musicVolume = 128;
	}
	if (value < 0)
		_musicVolume = 0;
	Mix_VolumeMusic(_musicVolume);
}

void AudioManager::increaseMusicVolume()
{
	if (_musicVolume < 128)
		++_musicVolume;
	Mix_VolumeMusic(_musicVolume);
}

void AudioManager::decreaseMusicVolume()
{
	if (_musicVolume > 0)
		--_musicVolume;
	Mix_VolumeMusic(_musicVolume);
}

void AudioManager::close()
{
 	Mix_FreeMusic(_music);
	_music = NULL;
	Mix_FreeChunk(_sfx);
	_sfx = NULL;
}
