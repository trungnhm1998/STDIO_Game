#include"Texture.h"
#include"GraphicManager.h"

Texture::Texture()
{
	_size = {0};
	_texture = nullptr;
}

Texture::~Texture()
{
	SDL_DestroyTexture(_texture);
	_texture = nullptr;
}

bool Texture::loadTextureFromPath(const char * sourcePath)
{
	//load image using surface first
	SDL_Surface* TempSurface = IMG_Load(sourcePath);
	if (TempSurface == nullptr)
	{
		printf("IMAGE count not load! Error: %s\n",IMG_GetError());
		return false;
	}
	else
	{
		//convert Surface pixel to renderer so GPU can draw
		_texture = SDL_CreateTextureFromSurface(GraphicManager::getInstance()->getRenderer(), TempSurface);
		if (_texture == nullptr)
		{
			printf("Unable to create texture from %s! SDL Error: %s\n",sourcePath,SDL_GetError());
		}
		else
		{
			SDL_QueryTexture(_texture, NULL, NULL, &_size.w, &_size.h);
			_centerPoint = { _size.w / 2,_size.h / 2 };
			SDL_FreeSurface(TempSurface);
			TempSurface = nullptr;
		}
	}

	return true;
}

SDL_Texture * Texture::getTexture()
{
	return _texture;
}

int Texture::getTextureWidth()
{
	return _size.w;
}

int Texture::getTextureHeight()
{
	return _size.h;
}

SDL_Point Texture::getCenterPoint()
{
	return SDL_Point();
}

void Texture::setTexture(SDL_Texture* texture)
{
	_texture = texture;
}

