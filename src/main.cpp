#include "Application.h"
#include "GamePlayScene.h"
int main(int argc, char* args[])
{
	Application Game;
	if (Game.init("My Game my SDL Engine",800,600))
	{
		SceneManager::getInstance()->setStartScene(new GamePlayScene);
		Game.run(60);
	}
	
	return 0;
}