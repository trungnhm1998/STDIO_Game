#include "GamePlayScene.h"

GamePlayScene::GamePlayScene()
{
}

GamePlayScene::~GamePlayScene()
{
	delete Mario;
	delete Map;
}

void GamePlayScene::init()
{
	Map = new TileMap();
	Map->loadMap("World1-1.json");

	Mario = new Animation();
	Mario->loadAnimation("smallMario.png","smallMario.json", 8, 1,3);
	Mario->loadTexture("smallMario.png","smallMario.json","SmallMarioStand.png");
	Mario->setScale(4.0f);
}

void GamePlayScene::update(Uint32 dt)
{
	if (InputManager::getInstance()->getKeyDown(KeyboardKey::KEY_SPACE))
		Mario->playAnimation();
	else
	{
		Mario->stopAnimation();
		Mario->loadTexture("smallMario.png", "smallMario.json", "SmallMarioStand.png");
	}
	if (InputManager::getInstance()->getKeyState(KeyboardKey::KEY_S))
		Mario->flipHorizontal();
	Mario->update(dt);
}

void GamePlayScene::draw()
{
	Map->drawLayer("Background");
	Mario->draw();
}
