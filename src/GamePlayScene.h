#pragma once
#include "Scene.h"

class GamePlayScene : public Scene
{
public:
	GamePlayScene();
	~GamePlayScene();
	virtual void init() override;
	virtual void update(Uint32 dt) override;
	virtual void draw() override;

private:
	Animation* Mario;
	TileMap* Map;
};

